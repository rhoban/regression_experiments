library(ggplot2)

cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

samples <- read.csv("samples.csv");
predictions <- read.csv("predictions.csv");

predictions$nbTrees = as.factor(predictions$nbTrees)

g <- ggplot()

# BW version
#g <- g + geom_line(data=predictions, aes(x=input,y=output, group=nbTrees, linetype=nbTrees))
# Color version
g <- g + geom_line(data=predictions, aes(x=input,y=output, group=nbTrees, color=nbTrees),size=1, alpha=1)
g <- g + scale_color_manual(values= cbbPalette)

g <- g + geom_point(data=samples, aes(x=input,y=output),
                    shape='+',
                    size=8)

g <- g + stat_function(data = data.frame(x=c(-pi,pi)),aes(x),fun=sin, alpha=0.2, size=2)

g <- g + theme_bw()

ggsave("forests_predictions_example.png", width=6,height=3)
